import React, { Component } from 'react';
import { Rnd } from 'react-rnd';
import Loader from 'react-loaders';
// Parts and modules
import IconMenu from './iconMenu/index';
import LeftMenu from './leftMenu/index';
import RightMenu from './rightMenu/index';
// Here comes the STYLE baby!
import '../static/editorStyle.scss';

export interface Props {}

interface State {
  rightMenuWidth: number;
  loading: boolean;
  minimized: boolean;
}

class GameEditor extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.minimize = this.minimize.bind(this);
    this.resizeStop = this.resizeStop.bind(this);

    this.state = {
      minimized: false,
      loading: true,
      rightMenuWidth: 200,
    };
  }

  componentWillMount() {}

  componentDidMount() {
    this.setState({ loading: false });
  }

  // componentDidUpdate(prevProps, prevState) {}

  minimize() {
    this.setState({ minimized: !this.state.minimized });
  }

  resizeStop(e: any, dir: string, refToElement: HTMLDivElement) {
    if (refToElement && refToElement.offsetWidth) {
      const width = refToElement.offsetWidth;
      this.setState({ rightMenuWidth: width });
      localStorage.setItem('rightMenuWidth', width.toString());
    }
  }

  render() {
    const { minimized, rightMenuWidth, loading } = this.state;

    return (
      <React.Fragment>
        <div className={`Loader${!loading ? ' hidden' : ''}`}>
          <Loader type="line-scale" active />
        </div>
        <div className="IconMenu">
          <IconMenu />
        </div>
        <Rnd
          className="LeftMenu"
          default={{
            x: 48,
            y: 0,
            width: 200,
            height: 'calc(100% - 24px)',
          }}
          minWidth={200}
          maxWidth={'60%'}
          bounds="window"
          disableDragging
          enableResizing={{
            top: false,
            right: true,
            bottom: false,
            left: false,
            topRight: false,
            bottomRight: false,
            bottomLeft: false,
            topLeft: false,
          }}
        >
          <LeftMenu />
        </Rnd>
        <div className="Editor">
          <div className={`centralSubMenu${minimized ? ' minimize' : ''}`}>
            <div className="minimizeBtn" onClick={this.minimize}>
              {!minimized ? 'minimize' : 'maximize'}
            </div>
            <p>SubMenu</p>
          </div>
        </div>
        <Rnd
          style={{ position: 'relative', display: 'block' }}
          className="RightMenu"
          default={{
            x: document.body.offsetWidth - 200,
            y: 0,
            width: 200,
            height: 'calc(100% - 24px)',
          }}
          size={{ width: rightMenuWidth, height: 'calc(100% - 24px)' }}
          position={{
            x: document.body.offsetWidth - rightMenuWidth,
            y: 0,
          }}
          minWidth={200}
          maxWidth={'60%'}
          bounds="window"
          disableDragging
          enableResizing={{
            top: false,
            right: false,
            bottom: false,
            left: true,
            topRight: false,
            bottomRight: false,
            bottomLeft: false,
            topLeft: false,
          }}
          onResizeStop={this.resizeStop}
        >
          <RightMenu rightMenuWidth={rightMenuWidth} />
        </Rnd>
      </React.Fragment>
    );
  }
}

export default GameEditor;

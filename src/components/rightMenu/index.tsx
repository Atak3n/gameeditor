import React, { Component } from 'react';
import GridLayout from 'react-grid-layout';

export interface Props {
  rightMenuWidth: number;
}

interface State {
  loading: boolean;
}

class RightMenu extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    // this.testF = this.testF.bind(this);

    this.state = {
      loading: true,
    };
  }

  componentWillMount() {}

  componentDidMount() {
    this.setState({ loading: false });
  }

  // componentDidUpdate(prevProps, prevState) {}

  render() {
    const { rightMenuWidth } = this.props;

    return (
      <GridLayout
        className="layout"
        isResizable
        isDraggable={false}
        autoSize
        margin={[1, 1]}
        cols={1}
        rowHeight={30}
        width={rightMenuWidth}
      >
        <div key="a" data-grid={{ x: 0, y: 0, w: 1, h: 1, static: true }}>
          Right Menu
        </div>
        <div key="b" data-grid={{ x: 0, y: 1, w: 1, h: 2, minW: 1, maxH: 4 }}>
          b (with max height)
        </div>
        <div key="c" data-grid={{ x: 0, y: 4, w: 1, h: 4 }}>
          c
        </div>
        <div key="d" data-grid={{ x: 9, y: 7, w: 1, h: 2 }}>
          d
        </div>
      </GridLayout>
    );
  }
}

export default RightMenu;

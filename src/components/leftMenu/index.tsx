import React, { Component } from 'react';
import Tree, { TreeNode } from 'rc-tree';
// import './draggable.less';

export interface Props {}

interface State {
  loading: boolean;
  autoExpandParent: boolean;
  expandedKeys: Array<string>;
  gData: any;
}

class LeftMenu extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    // this.testF = this.testF.bind(this);

    this.state = {
      loading: true,
      gData: [
        {
          title: 'Свет',
          key: '0-1-0-key',
          children: [
            {
              title: 'Источник 1',
              key: '0-1-0-0-key',
            },
            {
              title: 'Источник 2',
              key: '0-1-0-1-key',
            },
          ],
        },
        {
          title: '0-1-1-label',
          key: '0-1-1-key',
          children: [
            { title: '0-0-0-0-label', key: '0-0-0-0-key' },
            { title: '0-0-0-1-label', key: '0-0-0-1-key' },
            { title: '0-0-0-2-label', key: '0-0-0-2-key' },
          ],
        },
        { title: '0-1-2-label', key: '0-1-2-key' },
      ],
      autoExpandParent: true,
      expandedKeys: ['0-0-key', '0-0-0-key', '0-0-0-0-key'],
    };
  }

  componentWillMount() {}

  componentDidMount() {
    this.setState({ loading: false });
  }

  // componentDidUpdate(prevProps, prevState) {}

  onDragStart = (info: any) => {
    console.log('start', info);
  };

  onDragEnter = (info: any) => {
    console.log('enter', info);
    this.setState({
      expandedKeys: info.expandedKeys,
    });
  };
  onDrop = (info: any) => {
    console.log('drop', info);
    const dropKey = info.node.props.eventKey;
    const dragKey = info.dragNode.props.eventKey;
    const dropPos = info.node.props.pos.split('-');
    const dropPosition =
      info.dropPosition - Number(dropPos[dropPos.length - 1]);

    const loop = (data: any, key: string, callback: any) => {
      data.forEach((item: any, index: any, arr: any) => {
        if (item.key === key) {
          callback(item, index, arr);
          return;
        }
        if (item.children) {
          loop(item.children, key, callback);
        }
      });
    };
    const data = [...this.state.gData];

    // Find dragObject
    let dragObj: any;
    loop(data, dragKey, (item: any, index: any, arr: any) => {
      arr.splice(index, 1);
      dragObj = item;
    });

    if (!info.dropToGap) {
      // Drop on the content
      loop(data, dropKey, (item: any) => {
        item.children = item.children || [];
        // where to insert 示例添加到尾部，可以是随意位置
        item.children.push(dragObj);
      });
    } else if (
      (info.node.props.children || []).length > 0 && // Has children
      info.node.props.expanded && // Is expanded
      dropPosition === 1 // On the bottom gap
    ) {
      loop(data, dropKey, (item: any) => {
        item.children = item.children || [];
        // where to insert 示例添加到尾部，可以是随意位置
        item.children.unshift(dragObj);
      });
    } else {
      // Drop on the gap
      let ar: any;
      let i: any;
      loop(data, dropKey, (item: any, index: any, arr: any) => {
        ar = arr;
        i = index;
      });
      if (dropPosition === -1) {
        ar.splice(i, 0, dragObj);
      } else {
        ar.splice(i + 1, 0, dragObj);
      }
    }

    this.setState({
      gData: data,
    });
  };
  onExpand = (expandedKeys: any) => {
    console.log('onExpand', expandedKeys);
    this.setState({
      expandedKeys,
      autoExpandParent: false,
    });
  };

  render() {
    const loop = (data: any) => {
      return data.map((item: any) => {
        if (item.children && item.children.length) {
          return (
            <TreeNode key={item.key} title={item.title}>
              {loop(item.children)}
            </TreeNode>
          );
        }
        return <TreeNode key={item.key} title={item.title} />;
      });
    };

    return (
      <Tree
        expandedKeys={this.state.expandedKeys}
        onExpand={this.onExpand}
        autoExpandParent={this.state.autoExpandParent}
        draggable
        multiple
        onDragStart={this.onDragStart}
        onDragEnter={this.onDragEnter}
        onDrop={this.onDrop}
      >
        {loop(this.state.gData)}
      </Tree>
    );
  }
}

export default LeftMenu;

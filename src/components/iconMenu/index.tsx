import React, { Component } from 'react';

export interface Props {}

interface State {
  loading: boolean;
}

class IconMenu extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      loading: true,
    };
  }

  componentWillMount() {}

  componentDidMount() {
    this.setState({ loading: false });
  }

  // componentDidUpdate(prevProps, prevState) {}

  render() {
    return (
      <div className="iconMenu">
        <p>Icon Menu</p>
        <div className="icon">1</div>
        <div className="icon">2</div>
        <div className="icon">3</div>
        <div className="icon">4</div>
        <div className="icon">5</div>
        <div className="icon">6</div>
        <div className="icon">7</div>
        <div className="icon">9</div>
      </div>
    );
  }
}

export default IconMenu;

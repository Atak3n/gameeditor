/* eslint-disable */
import React from 'react';
import './App.scss';
import '../node_modules/react-grid-layout/css/styles.css';
import '../node_modules/react-resizable/css/styles.css';

import GameEditor from './components/index';

function App() {
  return (
    <div className="App">
      <GameEditor />
      <div className="Console">TestConsole</div>
    </div>
  );
}

export default App;
